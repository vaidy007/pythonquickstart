#! /opt/csw/bin/python3
##################################################################################################
# Simplify some of the everyday pac queries. 
# This is the mrun script for SAs ;-)
#
# Behaviour: 
# for the rows matching the criteria:
#   def display just the Hostname.
#   -v  display: Hostname, Lifecycle, (short)location, support group, status.
#   -vv display: Hostname, Lifecycle, (short)location, support group, status, OS Version, Model.
#   -a  display: dump the entire line
#
# Criteria:
#   The remaining arguments will be matched against the remaining arguments 
#    - Fuzzy match?
#    - as regular expressions?
# 

import argparse
import csv
import sys
import os
import re
import mmap
import contextlib
pacfile="/etc/hosts"
#pacfile="/home/pacman/ALL_PAC.TXT"

"""
>>> pacreader.fieldnames for the pacfile:
['Hostname', 'Lifecycle', 'Location', 'Console Server Port', 'IP Type', 'Support Group', 'Status', 'OpSys', 'OS Version', 'IP Address', 'Brand', 'Model', 'Server CPUs', 'Server CPU Speed', 'Server RAM', 'Serial Number', 'In DMZ', 'Acquis Cost Centre', 'HCL Access', 'Application Technical Contact', 'Host Flag', 'Parent', 'CIO Name', 'Secondary Support Group', 'Cabinet Name', 'TRM_HW_Support', 'TRM_SW_Support', 'Subcategory', 'NAR ID']
"""

#### Subroutines
###
def pacprint(row,verbosity):
	if verbosity == 1:
		print("%s,%s,%s,%s" % (row['Hostname'],row['Lifecycle'],row['Location'],row['Support Group']))
	elif verbosity == 2:
		print(row)
	else:
		print(row['Hostname'])

##### main
###
def main():
	parser = argparse.ArgumentParser(description="Vaidy's parser for the pac file")
	parser.add_argument("-v","--verbose",action="count",help="Increase number of fields displayed")
	parser.add_argument('look_for',nargs='*',help="Space delimited values to look for")
	args = parser.parse_args()

	try:
		with open(pacfile) as f:
			m = mmap.mmap(f.fileno(),0,prot=mmap.PROT_READ)
			for line in iter(m.readline, ""):
				print(line.decode('utf-8'))
			m.close()

	except Error as err:
		print("OS error: {0}".format(err))	
		sys.exit(2)

if __name__ == '__main__':
        try:
                main()
        except KeyboardInterrupt:
                print ('Interupted')
                try:
                        sys.exit(0)
                except SystemExit:
                        os._exit(0)
