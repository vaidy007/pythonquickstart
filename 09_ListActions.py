import sys
import os

###### Subroutines #######

def drawline():
	print("-"*80)

def ListComprehension():
	print("\n\nList comprehension generates an iterable list based on certain conditions.")
	drawline()

	print("Get even numbers to 10: [i for i in range(11) if i % 2 == 0]")
	evens_to_10 = [i for i in range(11) if i % 2 == 0]
	print (evens_to_10)

	print("Get doubles of numbers 1-6: [x*2 for x in range(1,6)]")
	doubles = [x*2 for x in range(1,6)]
	print (doubles)

	print("Get [x*2 for x in range(1,6) if (x*2) % 3 == 0]")
	doubles_by_3 = [x*2 for x in range(1,6) if (x*2) % 3 == 0]
	print (doubles_by_3)



def ListSlicing():
	print("\n\nList slicing gets sections of a list based on certain conditions.")
	drawline()

	print("Given a list lst=[A, B, C, D, E, F, G, H, I, J]")
	lst=["A","B","C","D","E","F","G","H","I","J"]

	print("listname[start:end:stride]    : lst[2:9:2] :", lst[2:9:2])
	print("Omit the first three values   : lst[3:]    :", lst[3:])
	print("Omit all after second value   : lst[:2]    :", lst[:2])
	print("skip every other value        : lst[::2]   :", lst[::2])
	print("print the list backwards      : lst[::-1]  :", lst[::-1])

	print("\nCount backwards from 100 by stride of 10")
	print("hun=range(101)                : hun[::-10] :", range(101)[::-10])

def ListEnumerate():
	print("\n\nList enumeration enumerates a list")
	drawline()
	print("Given a list choices=['pizza', 'pasta', 'salad', 'nachos']")
	choices=['pizza', 'pasta', 'salad', 'nachos']
	print("enumerate(list) will return index,item values:")
	for index,item in enumerate(choices):
		print(index,item)

def ListZip():
	print("\n\n List ZIP lets you walk multiple lists in parallel")
	drawline()
	print("list_a = ['10','2','30','4']")
	print("list_b = ['1','20','3','40','5','60']")
	print("Using zip(list_a,list_b) you can compare corressponding elements")
	print("For example: print the larger of the two numbers")
	list_a = ['10','2','30','4']
	list_b = ['1','20','3','40','5','60']
	for a,b in zip(list_a,list_b):
		if a > b:
			print (a)
		else:
			print (b)

###### MAIN ########

def main():
	ListComprehension()
	ListSlicing()
	ListEnumerate()
	ListZip()

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print ('Interupted')
		try:
			sys.exit(0)
		except SystemExit:
			os._exit(0)

