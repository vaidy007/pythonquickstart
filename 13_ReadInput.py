import sys
import os

def ReadFromKeyboard():
	GetInput = True
	while(GetInput):
		name=input("Enter name: ")
		age=input("Enter age: ")
		if(age.isnumeric()):
			print("Hello %s, who is %d yrs young" % (name,int(age)))
			GetInput = False
		else:
			print("Age should be numeric. Reenter all data")



###### MAIN ########

def main():

	print(''' 
Reminder: All input from keyboard is read as string and 
needs to be converted to the correct type.
	''')

	ReadFromKeyboard()


if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print ('Interupted')
		try:
			sys.exit(0)
		except SystemExit:
			os._exit(0)

