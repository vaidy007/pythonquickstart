import sys
import os

###### Subroutines #######

def DoBuiltin():
	print("Built In functions")
	print("The minimum of 4,5,6 is min(4,5,6)  :" , min(4,5,6))
	print("The maximum of 4,5,6 is max(4,5,6)  :" , max(4,5,6))
	print("The absolute value of -6 is abs(-6) :" , abs(-6))

###### MAIN ########

def main():
	DoBuiltin()

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print ('Interupted')
		try:
			sys.exit(0)
		except SystemExit:
			os._exit(0)

