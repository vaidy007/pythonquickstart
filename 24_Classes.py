## Simple Class
class ShoppingCart(object):

    items_in_cart = {}

    def __init__(self, customer_name):
        self.customer_name = customer_name

    def add_item(self, product, price):
        """Add product to the cart."""
        if not product in self.items_in_cart:
            self.items_in_cart[product] = price
            print(product + " added.")
        else:
            print(product + " is already in the cart.")

    def remove_item(self, product):
        """Remove product from the cart."""
        if product in self.items_in_cart:
            del self.items_in_cart[product]
            print(product + " removed.")
        else:
            print(product + " is not in the cart.")

custId = "Bla"
print("Initialize an instance of the class ShoppingCart")
mycart = ShoppingCart("custId")
print("Add an item to this customers cart")
mycart.add_item("banana",0.20)
for key in mycart.items_in_cart:
	print("Cost of ",key,"in shopping cart:", mycart.items_in_cart[key], "dollars")
 
##Inheritance:

class Customer(object):
    """Produces objects that represent customers."""
    def __init__(self, customer_id):
        self.customer_id = customer_id

    def display_cart(self):
        print("I'm a string that stands in for the contents of your shopping cart!")

class ReturningCustomer(Customer):
    """For customers of the repeat variety."""
    def display_order_history(self):
        print("I'm a string that stands in for your order history!")

monty_python = ReturningCustomer("ID: 12345")
monty_python.display_cart()
monty_python.display_order_history()


## override
class Employee(object):
    def __init__(self, name):
        self.name = name
    def greet(self, other):
        print("Hello, %s" % other.name)

class CEO(Employee):
    def greet(self, other):
        print("Get back to work, %s!" % other.name)

ceo = CEO("Emily")
emp = Employee("Steve")
emp.greet(ceo)
ceo.greet(emp)

## Super Call
class Employee(object):
    """Models real-life employees!"""
    def __init__(self, employee_name):
        self.employee_name = employee_name

    def calculate_wage(self, hours):
        self.hours = hours
        return hours * 20.00

# Add your code below!
class PartTimeEmployee(Employee):
    def calculate_wage(self,hours):
        self.hours = hours
        return hours * 12.00
    def full_time_wage(self,hours):
        return super(PartTimeEmployee,self).calculate_wage(hours)
        
milton = PartTimeEmployee("milton")
print(milton.full_time_wage(10))
