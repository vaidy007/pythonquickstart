import sys
import os

###### Subroutines #######

def drawline():
	print("-"*80)

def DoSomeMath():
	drawline()
	print("Addition      :  	13  +  12     	: " , 13+12)
	print("Concatenation :  	'abc'+'def'   	: " , 'abc'+'def')
	print("Subtraction   :  	13  -  12     	: " , 13-12)
	print("Multiplication:  	13  *  12     	: " , 13*12)
	print("Division      :  	12  *   4     	: " , 12/4)
	print("Modulo        : 	       108  *  10     	: " , 108%10)
	print("exponential   :  	10 **   2     	: " , 10**2)
	print("Remainder     :   	 5 //   3     	: " ,  5//3)
	drawline()
	print("Note: (2 + 3) / 2 is not the same as (2 + 3) / 2.0! ")
	print("The former is integer division, meaning you get an integer back. ")
	drawline()

def DoCompare():
        drawline()
        print ("5 == 5 is" , 5 == 5 )
        print ("6 != 5 is" , 6 != 5 )
        print ("4 <  5 is" , 4 < 5 )
        print ("4 <= 5 is" , 4 <= 5 )
        print ("8 >  5 is" , 8 > 5 )
        print ("8 >= 5 is" , 8 >= 5 )
	drawline()
        print ("not: is evaluated first")
        print ("and: is evaluated next")
        print (" or: is evaluated last")
        drawline()


def DoBitWise():
	drawline()
	print ("Right Shift :	255 >> 4	: " , 255 >> 4 )
	print ("Left Shift  :	  8 << 4	: " ,   8 << 4 )
	print ("Bitwise AND :	  8  & 5	: " ,   8 & 5 )
	print ("Bitwise OR  :	  8  | 5	: " ,   8 | 5 )
	print ("Bitwise XOR :	  8  ^ 5	: " ,   8 ^ 5 )
	print ("Bitwise NOT :	  ~88		: " ,   ~88 )
	print ("Bitwise rep :   0b111 = 7       : " ,   0b111)
	print ("Int to bin  :   bin(7)		: " ,   bin(7))
	print ("bin to int  :   int(0b100)      : " ,   int(0b100))
	drawline()

###### MAIN ########

def main():
	DoSomeMath()
	DoCompare()
	DoBitWise()

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print ('Interupted')
		try:
			sys.exit(0)
		except SystemExit:
			os._exit(0)

