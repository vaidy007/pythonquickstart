import sys
import os

###### Subroutines #######

def DoWhile():

	print(''' 
1) Basic Structure of a While Loop:

count = 1
while count < 10:
	print("Will print while %d is less than 10" % count)
	count += 1
else:
	print("Now %d is more than 10. So we are out of the" % count)
	print("main while loop and running the else part.")

Output:
	''')

	count = 1
	while count < 10:
		print("Will print while %d is less than 10" % count)
		count += 1
	else:
		print("Now %d is more than 10. So we are out of the main while loop and running the else part." % count)

	print('''
2) While True loop:
x = True
while x:
	print("This loop will run once. Setting x to False")
	x = False

Output:
	''')

	x = True
	while x:
		print("This loop will run once. Setting x to False")
		x = False

###### MAIN ########

def main():
	DoWhile()

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print ('Interupted')
		try:
			sys.exit(0)
		except SystemExit:
			os._exit(0)

