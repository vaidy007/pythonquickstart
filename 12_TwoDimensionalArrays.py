import sys
import os

###### Subroutines #######

def drawline():
        print("-"*80)


def TwoDimensionalArrays():
	print("[\"O\"]*5] : ")
	print(["O"]*5)

	board = []
	for number in range(5):
		board.append(["O"] * 5)

	for row in range(5):
		for col in range(5):
			print(board[row][col],end=" ")
		print("\n")


###### MAIN ########

def main():
	TwoDimensionalArrays()

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print ('Interupted')
		try:
			sys.exit(0)
		except SystemExit:
			os._exit(0)

