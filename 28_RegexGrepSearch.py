print("")
print("1) Find value within list")
print("")
# List of string 
listOfStrings = ['Hi' , 'hello', 'at', 'this', 'there', 'from','at']
print(listOfStrings)
if 'at' in listOfStrings :
    print("Yes, 'at' found in List : " , listOfStrings)
if 'time' not in listOfStrings :
    print("Yes, 'time' NOT found in List : " , listOfStrings)
if listOfStrings.count('at') > 0 :
    print("Yes, 'at' found in List %d times: " , listOfStrings.count('at'))
print("")

print("2) Check if Dictionary[key] exists")
print("")
dic = {'1': 'one', '3': 'three', '2': 'two', '5': 'five', '4': 'four'}
print("2a) using \"value in dic.value() or \"key in dic.keys()\"" )
print('one' in dic.values())
print('1' in dic.keys())

if 'one' in dic.values():
    print("Found %s in dic" % "one")
else:
    print("Did not find one in values")
print("")
print("2b) using \"dic.get(key)\"")
print(dic.get('1'))
print(dic.get('10'))
print("")
print("3) grep for value in a string using regular expressions")
print("")
# import the additional module
import re
# define list of places
listOfPlaces = ["Bayswater", "Table Bay", "Bejing", "Bombay"]
print(listOfPlaces)
# define search string
pattern = re.compile("[Bb]ay")
for place in listOfPlaces:
    if pattern.search(place):
        print ("%s matches the search pattern" % place)