import sys
import os
import argparse

###### MAIN ########

	### Initialize ArgumentParser (can have a description with the description=)
parser = argparse.ArgumentParser(description="Calculate the square of a number")

	## Positional arguments
	### All values are strings unless specified with type=int
parser.add_argument("square",help="The number you want the square for",type=int)

	## Optional arguments
	##1## As a True / False value ( action="store_true")
parser.add_argument("-v","--verbose",help="Increase output verbosity",action="store_true")
	##2## With a value but limited to specific choices (choices=)
parser.add_argument("-x","--xerbose",help="Increase output xerbosity by level",type=int,choices=[0,1,2])
	##3## With the number of times the short form is used (action="count")
	##4## set a default value with (default=)
parser.add_argument("-a","--aerbose",help="Increase output aerbosity by level",action="count",default=0)
	### Group arguments together
group = parser.add_mutually_exclusive_group()
group.add_argument("-b", "--berbose", action="store_true")
group.add_argument("-q", "--quiet", action="store_true")

args = parser.parse_args()

answer =  args.square**2
if args.verbose >= 2:
    print("the square of {} equals {}".format(args.square, answer))
elif args.verbose >= 1:
    print("{}^2 == {}".format(args.square, answer))
else:
    print(answer)

