print("#1: importing threading and queue modules")
import threading
from queue import Queue
import time
import datetime

def ptime():
    return (datetime.datetime.now().strftime("%H:%M:%S.%f"))

#7 The actual work
def exampleJob(task):
    with print_lock:
        print("#7 exampleJob: %s : This is thread name %s working on task %d \n The task is to sleep for 1.5 seconds." % (ptime(),threading.current_thread().name,task))
        time.sleep(1.5)  


#6 Enter the actual Threading operation function (threader)
#6a Continue till the main thread goes on..so:
def threader():
    while True:
        task = q.get() # Get a worker from the queue.
        print("#6 threader: %s : thread name %s got task %d" % (ptime(),threading.current_thread().name, task))
        exampleJob(task) # Run the job with this worker.
        q.task_done() # completed with the job.
         

#2 We begin with defining locks.
#2 Everything you do with threads should be locked.
#2 This includes variables/print to screen etc.
print("#2: defining the threading locks")
print_lock = threading.Lock()

#3 Define the queue (instance of the class Queue)
print ("#3 Creating an instance of the class Queue")
q = Queue()

#4a define the # of threads and what each of these threads to do (target=).
#4b Classify each  thread as a daemon, so the thread will die with main dies..
#4c Start each thread.

for x in range(2):
    print ("#4-%d Defining Thread number %d" % (x,x)) 
    t = threading.Thread(target = threader)
    t.daemon = True
    t.start()

start = time.time()

#5 Assign some "tasks" (20 in our case). Put the threads to work.
#5 So 10 workers will do these 20 tasks.
for task in range(1,5):
    print ("#5-%d Defining task number %d" % (task,task))
    q.put(task)
    
#5b Wait till the thread terminates.
q.join() 

#8 You get here once all the threads are done
print("8. All tasks are completed")
print('Entire job took:',time.time()-start)

