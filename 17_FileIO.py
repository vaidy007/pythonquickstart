import sys
import os

###### Subroutines #######

'''
- 'r' when the file will only be read 
- 'w' for only writing (an existing file with the same name will be erased), 
- 'a' opens the file for appending; any data written to the file is automatically added to the end. 
- 'r+' opens the file for both reading and writing.
'''

def drawline():
        print("-"*80)


def FileIO():
	linenum = 1
	drawline()
	print("Opening : /etc/issue : read-only")
	try:
		f = open("/etc/issue","r")
	except OSError as err:
		print("OS error: {0}".format(err))
	drawline()

	print(">>>>>>>> Entire file contents")
	print(f.read())

	drawline()
	print("<<<<<<<< Go back to start of file - f.seek(0)")
	f.seek(0)
	print(">>>>>>>> Read file one line at a time")
	drawline()
	for line in f:
		print(linenum,":",line,end="")
		linenum += 1

	drawline()
	print("<<<<<<<< Go back to start of file - f.seek(0)")
	f.seek(0)
	print(">>>>>>>> Read file into array")
	drawline()
	FileAsList = f.readlines()
	print(FileAsList)

	drawline()
	print("<<<<<<<< Go back to start of file - f.seek(0)")
	f.seek(0)
	print(">>>>>>>> Read file into array")
	drawline()
	FileAsList = list(f)
	print(FileAsList)
	drawline()

	f.close()

	filestring = input("Enter a string: ")
	f = open("/tmp/bla","w")
	f.write(filestring)
	f.write("\n")
	f.close()

###### MAIN ########

def main():
	FileIO()

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print ('Interupted')
		try:
			sys.exit(0)
		except SystemExit:
			os._exit(0)

