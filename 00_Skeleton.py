import sys
import os

###### Subroutines #######

def drawline():
        print("-"*80)


def DoNothing():
	pass


###### MAIN ########

def main():
	print("Skeleton Python Script")
	DoNothing()

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print ('Interupted')
		try:
			sys.exit(0)
		except SystemExit:
			os._exit(0)

