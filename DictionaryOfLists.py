import sys
import os

###### Subroutines #######

def drawline():
        print("-"*80)


def DictOfList():
	inventory = {
	'gold' : 500,
	'pouch' : ['flint', 'twine', 'gemstone'], # Assigned a new list to 'pouch' key
	'backpack' : ['xylophone','dagger', 'bedroll','bread loaf']
	}
	print("Dictionary of Lists: ")

	for key in inventory:
		print("\t",key,inventory[key])

	print("List of pouch")
	print ("\t",inventory["pouch"][0])

	print("Adding a key 'burlap bag' and assigning a list to it")
	inventory['burlap bag'] = ['apple', 'small ruby', 'three-toed sloth']
	for key in inventory:
		print("\t",key,inventory[key])

	print("Sorting the list found under the key 'pouch'")
	inventory['pouch'].sort() 
	for key in inventory:
		print("\t",key,inventory[key])

	print("Add another key, sort the backpack and remove the dagger")
	inventory['pocket'] = ['seashell','strange berry', 'lint']
	inventory['backpack'].sort()
	inventory['backpack'].remove('dagger')
	inventory['gold'] = inventory['gold'] + 50

	print("Test if value exists, if not initialize it as a list")
	if inventory.get('picnic') is None:
		inventory['picnic'] = []
		inventory['picnic'].append('Sandwich')
	else:
		inventory['picnic'].append('Water')

	for key in inventory:
		print("\t",key,inventory[key])



###### MAIN ########

def main():
	DictOfList()

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print ('Interupted')
		try:
			sys.exit(0)
		except SystemExit:
			os._exit(0)

