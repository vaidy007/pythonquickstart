import sys
import os
import time
import datetime

###### Subroutines #######

def GetTime():
	now = time.time()
	future = now + 2
	while time.time() < future:
		time.sleep(0.00001)
		print(datetime.datetime.now().strftime("%H:%M:%S.%f"))	

def GetTimeNow():
	print("Epoch time now is time.time(): " , time.time())
	print("Print time now is datetime.datetime.now().strftime(\"%H:%M:%S.%f\"): " , datetime.datetime.now().strftime("%H:%M:%S.%f"))


###### MAIN ########

def main():
	GetTimeNow()

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print ('Interupted')
		try:
			sys.exit(0)
		except SystemExit:
			os._exit(0)

