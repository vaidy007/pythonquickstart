##### Import modules
###
import subprocess
import argparse
import socket
import ipaddress
import os
import sys

##### Subroutines
###

def drawline():
	print('-'*80)

def pinger(ipaddr):
        try:
                subprocess.check_call(
                                        ("ping " + str(ipaddr) + " 1 1").split(),
                                        stdout=subprocess.DEVNULL,
                                        stderr=subprocess.DEVNULL)
                return "Alive"
        except subprocess.CalledProcessError:
                return "Dead"
        except OSError as err:
                print("OS ERROR:{0}".format(err))


def FindByIp(ip2trace,subnet,skip):
	ip2trace = ip2trace + "/" + subnet
	try:
	  thishostip = ipaddress.ip_interface(ip2trace)
	except ValueError as err:
		print("ERROR:{0}".format(err)) 
		sys.exit(2)
	drawline()
	print("| %16s | %50s | %6s |" % ("IPADDRESS","HOSTNAME","PING"))
	drawline()
	for nextip in (thishostip.network.hosts()):
		try:
			if skip:
				pingresult="skip"
			else:
				pingresult=pinger(str(nextip))
			(hostname,aliaslist,ipaddrlist) = socket.gethostbyaddr(str(nextip))
			print ("| %16s | %50s | %6s |" % ("".join(ipaddrlist),hostname,pingresult))
		except socket.herror:
			print("| %16s | %50s | %6s |" % (str(nextip),"UNUSED","UNUSED"))
		except OSError as err:
                	print("OS ERROR:{0}".format(err))
	drawline()	

def FindByName(svr,subnet,skip):
	try:
		ip2trace = socket.gethostbyname(svr)
		FindByIp(ip2trace,subnet,skip)
	except socket.herror as herr:
		print("ERROR:{0}".format(herr))
	except OSError as err:
		print("OS ERROR:{}:{}".format(svr,err))

##### main
###
def main():
	parser = argparse.ArgumentParser(description="Get subnet list for given IP or hostname")
	group = parser.add_mutually_exclusive_group()
	group.add_argument("-s","--svr",help="Example: tokeqappp1")
	group.add_argument("-i","--IP",help="Example: 10.177.80.20")
	parser.add_argument("-m","--mask",help="Example: 26 or 255.255.255.192",default="24")
	parser.add_argument("-n","--skip",help="Skip the ping test",action="store_true")
	args = parser.parse_args()
	if args.svr:
		FindByName(args.svr,args.mask,args.skip)
	if args.IP:
		FindByIp(args.IP,args.mask,args.skip)

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print ('Interupted')
		try:
			sys.exit(0)
		except SystemExit:
			os._exit(0)



