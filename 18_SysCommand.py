import sys
import os
import subprocess

###### Subroutines #######

def drawline():
        print("-"*80)

def SubProcess():
	subprocess.call(["ls", "-l"])
	subprocess.check_call(["ls", "-l"])

def pinger(ipaddr):
	try:
		subprocess.check_call(
			("ping " + str(ipaddr) + " 2 2").split(),
			stdout=subprocess.DEVNULL,
			stderr=subprocess.DEVNULL)
		return "Alive"
	except subprocess.CalledProcessError:
		return "Dead"
	except OSError as err:
		print("OS ERROR:{0}".format(err))
	


###### MAIN ########

def main():
	SubProcess()
	if len(sys.argv) <= 1:
		print("Enter IP address to ping")
	else:
		print(pinger(sys.argv[1]))

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print ('Interupted')
		try:
			sys.exit(0)
		except SystemExit:
			os._exit(0)

