import sys
import os

###### Subroutines #######

def ListsArrays():
	print("\n\nQuick examples of lists and their usage\n\n")

	print("1. Simple assignments to lists and manipulation")
	print("\tCreate list of animals:")
	print('\t\tanimals = ["aardvark", "badger", "duck", "emu"]')

	animals = ["aardvark", "badger", "duck", "emu"]
	print("\t\tlen(animals) is now",len(animals))

	print("\tanimals.append(cat) into the list")
	animals.append("cat")
	print("\t\tlen(animals) is now",len(animals))

	print('\tduck is at index number animals.index("duck"):' , animals.index("duck"))
	di=animals.index("duck")
	print("\tanimals.insert(di,\"cobra\")") 
	animals.insert(animals.index("duck"),"cobra")
	print("\t\tThe list of animals now" , animals)

	print('\tanimals.remove("badger") from the list')
	animals.remove("badger")
	print("\t\tThe list of animals now" , animals)

	print("\n2.Iterate through a list")
	for animal in animals:
		print ("\t",animal)

	print("\tUse lists as stacks - LIFO - animals.pop()")
	print("\t\t", animals.pop())

	print ("\n3.Strings are Lists without a separator.")
	print ('\tLet\'s say animals = "catdogfrog"')
	animals = "catdogfrog"
	print("\t\tthen, animals[:3] will be:", animals[:3])
	print("\t\tand, animals[3:6] will be:", animals[3:6])
	print("\t\tand, animals[6:] will be:", animals[6:])

	print("\n4. \"delimiter\".join(array) to convert into string")
	print("\tletters=['w','o','r','d']")
	letters=['w','o','r','d']
	print("\tJoin with space:" , "".join(letters))
	print("\tJoin with comma:" , ",".join(letters))


###### MAIN ########

def main():
	ListsArrays()

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print ('Interupted')
		try:
			sys.exit(0)
		except SystemExit:
			os._exit(0)

