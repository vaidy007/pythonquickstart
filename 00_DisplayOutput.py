import sys
import os

# one line Comments

''' Multi
line
comments '''

###### Subroutines #######

def printstuff():
# Basic variable types

	print('''
0) Define some variables:
thisstr="This is a string"
thisword="abcd"
anotherword="efgh"
thisinteger = 8
thisfloat = 2.05
thisboolean = True''')

	thisstr="This is a string"
	thisword="abcd"
	anotherword="efgh"
	thisinteger = 8
	thisfloat = 2.05
	thisboolean = True


# Printing
	print("")
	print("1) print(\"This is basic string printing\")")
	print("This is basic string printing")
	print("")
	print("2) print(\"printing variable comtaining a string: %s\" % thisstr)")
	print("printing variable comtaining a string: %s" % thisstr)
	print("")
	print("3) print(\"a string without a newline\",end=\"\")")
	print("a string without a newline",end="")
	print("\n")
	print("4) print(\"This word %s, this number %d, and this float %f\" % (thisword,thisinteger,thisfloat))")
	print("This word %s, this number %d, and this float %f" % (thisword,thisinteger,thisfloat))
	print("")
	print("5) print(\"Fatal Error to stderr\", file=sys.stderr)")
	print("Fatal Error to stderr", file=sys.stderr)
	print("")
	print("6) print(thisword+anotherword)")
	print(thisword+anotherword)
	print("")
	print("7) print(thisword,anotherword)")
	print(thisword,anotherword)
	print("")
	print("8) print(thisword + \",\" + anotherword)")
	print(thisword + "," + anotherword)

# Pretty Printing
	print("")
	print("9) print ('{0}{1}{0}'.format('abra','-cad-'))")
	print ('{0}{1}{0}'.format('abra','-cad-'))
	print("")
	print('''10) print(\'''This is a way to print multiple lines
	using the multiline comment tags\''')''')
	print('''This is a way to print multiple lines
	using the multiline comment tags''')
	print("")

###### MAIN ########

def main():
	printstuff()


if __name__ == '__main__':
        try:
                main()
        except KeyboardInterrupt:
                print ('Interupted')
                try:
                        sys.exit(0)
                except SystemExit:
                        os._exit(0)

