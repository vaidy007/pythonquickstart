import sys
import os

###### Subroutines #######

def UseIfForNum(x):
	print("if x > 5")
	if x > 5:
		return True
	elif x < 5:
		return False
	else:
		return "Same"
	
def UseIfForString(mycity):	

	listOfPlaces = ["Berlin", "Paris", "Tokyo"]
	for place in listOfPlaces:
		if place < mycity:
			print ("%s comes before %s" % (place, mycity))
		elif place > mycity:
			print ("%s comes after %s" % (place, mycity))
		else:
			print ("%s is similar to %s" % (place, mycity))
	print("")
	print("if NewYork == NewYork:")
	if mycity.lower() == "NewYork".lower():
		return True
	else:
		return "False"


###### MAIN ########

def main():
	print("\nBasic structure of IF ELIF ELSE:")
	print('''
if condition:
	do_something
elif condition:
	do_something_else
else:
	do_another_thing''')
	print("")
	print("Checking if 10 is greater than 5: ", UseIfForNum(10))
	print("")
	print("Checking if 'newyork' is same as 'NewYork': ", UseIfForString("newyork"))
	print("")
	

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print ('Interupted')
		try:
			sys.exit(0)
		except SystemExit:
			os._exit(0)

