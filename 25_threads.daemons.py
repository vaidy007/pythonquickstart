import logging
import threading
import time

"""
The main thread will not wait for daemon threads to exit.
Daemon threads is useful for services where there may not be an easy way to interrupt the thread or where letting the thread die in the middle of its work does not lose or corrupt data (for example, a thread that generates heart beats for monitoring)
To force main to wait for daemon threads, use threadObject.join().
use threadObject.join() can have a time limit.
"""

logging.basicConfig(level=logging.DEBUG,format='[%(levelname)s] (%(threadName)-10s) %(message)s',)

def daemon():
	logging.debug('Starting')
	time.sleep(2)
	logging.debug('Exiting')

def non_daemon():
	logging.debug('Starting')
	logging.debug('Exiting')

d = threading.Thread(name='daemon', target=daemon)
d.setDaemon(True)
t = threading.Thread(name='non-daemon', target=non_daemon)

d.start()
t.start()

d.join(1)
print ("d.isAlive()", d.isAlive())
t.join()
