import sys
import os

###### Subroutines #######

def DrawLine():
	print("print(\"-\"*80)")
	print("-"*80)

###### MAIN ########

def main():
	DrawLine()

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print ('Interupted')
		try:
			sys.exit(0)
		except SystemExit:
			os._exit(0)

