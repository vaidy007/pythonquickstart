import sys
import os

###### Subroutines #######

def drawline():
        print("-"*80)

def Dictionary():
	print("\n\nDictionary are associative arrays")
	drawline()
	print("Define Dictionary: menu = {}")
	menu = {}
	print("add key,element pair at definition: menu = {'unicorn':'10.00'}")
	menu = {'unicorn':'10.00'}
	print("add key,element pair: menu['Chicken Alfredo'] = 14.50")
	menu['Chicken Alfredo'] = 14.50
	print("del key,element pair: del menu['Chicken Alfredo']")
	del menu['Chicken Alfredo']

	print("print menu.items()")
	print(menu.items())
	print("print menu.values()")
	print(menu.values())
	print("walk through an dictionary")
	for key in menu:
		print(key, menu[key])


###### MAIN ########

def main():
	Dictionary()

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print ('Interupted')
		try:
			sys.exit(0)
		except SystemExit:
			os._exit(0)

