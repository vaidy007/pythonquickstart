import sys
import os

#Import smtplib for the actual sending function
import smtplib
# Import the email modules we'll need
from email.mime.text import MIMEText

###### Subroutines #######

def drawline():
        print("-"*80)


def SendSimpleEmail():
	me = "vaidy.ganapathy@sender.com"
	you = "vaidy.ganapathy@rcvr.com"

	textfile = '/tmp/bla.txt'
	fp = open(textfile,'rb')
	msg = MIMEText(fp.read())
	fp.close()

	msg['From'] =  me
	msg['To'] = you
	msg['Subject'] = 'The contents of %s' % textfile

	s = smtplib.SMTP('localhost')
	s.send_message(msg)
	s.quit()
	


###### MAIN ########

def main():
	SendSimpleEmail()

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print ('Interupted')
		try:
			sys.exit(0)
		except SystemExit:
			os._exit(0)

