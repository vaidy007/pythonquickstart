print("""
Method 1
--------
When a module named spam is imported, the interpreter first 
searches for a built-in module with that name. If not found, 
it then searches for a file named spam.py in a list of 
directories given by the variable sys.path. 

sys.path is initialized from these locations:
	- The directory containing the input script or current directory.
	- PYTHONPATH (a list of directory names, with the same syntax as the shell variable PATH).
	- The installation-dependent default.

Method 2
--------
>>> import sys
>>> sys.path.append('/ufs/guido/lib/python')
""")
