import sys
import os

###### Subroutines #######

def ShowcaseStrings():

	print("\n\nBasic String Operations\n\n")

	st = "This Is A String"
	anotherst = "ThisIsAnotherString"
	csvst = "This,is,a,comma,seperated,string"
	StringWithSpaces = "   Space on both sides of string  "

	print(st)

	print("\tThe 4th char is                  :st[3]     :" , st[3])
	print("\tLength of string                 :len(st)   :" , len(st))

	print("\tConverted to lower               :st.lower():" , st.lower())
	print("\tConverted to upper               :st.upper():" , st.upper())

	print("\tCheck if the string isaplha      :st.isalpha():" , st.isalpha())

	print("")
	print(st)
	print("\tConverting the string into a list:", st.split())

	print("")
	print(csvst)
	print("\tConverting csv into a list:" ,csvst.split(','))

	print("\nConcatenate strings " + "using " + "+" + " between the strings\n")

	print("Remove spaces from " + ">" + StringWithSpaces + "<")
	StringWithSpaces = StringWithSpaces.lstrip()
	StringWithSpaces = StringWithSpaces.rstrip()
	print("Removed spaces" + ">" + StringWithSpaces + "<")



###### MAIN ########

def main():

	ShowcaseStrings()

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print ('Interupted')
		try:
			sys.exit(0)
		except SystemExit:
			os._exit(0)

