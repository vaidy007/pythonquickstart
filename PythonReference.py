
import sys
import os
import re
import inspect
import DictionaryOfLists

###### Quick Reference Subroutines #######
def Drawline():
    print("-"*80)

def DoNothing():
    pass

def PrintStuff():
# Basic variable types
	thisstr="This is a string"
	thisword="abcd"
	anotherword="efgh"
	thisinteger = 8
	thisfloat = 2.05
	thisboolean = True
# Simple Printing
	print("This is basic string printing")
	print("printing variable comtaining a string: %s" % thisstr)
	print("a string without a newline",end="")
	print("This word %s, this number %d, and this float %f" % (thisword,thisinteger,thisfloat))
	print("Fatal Error to stderr", file=sys.stderr)
	print(thisword+anotherword)
	print(thisword,anotherword)
	print(thisword + "," + anotherword)
# Pretty Printing
	print ('{0}{1}{0}'.format('abra','-cad-'))
	print('''This is a way to print multiple lines
	using the multiline comment tags''')


def ReadFromKeyboard():
    GetInput = True
    while(GetInput):
        name = input("Enter name: ")
        age = input("Enter age: ")
        if(age.isnumeric()):
            print("Hello %s, who is %d yrs young" % (name, int(age)))
            GetInput = False
        else:
            print("Age should be numeric. Reenter all data")

# STOP
####
# Functions are part of this program - no need to display these.
####

def DisplayMenu(menu):
    menu["q"] = "Quit"
    answer = "OK"
    while answer != "q":
        for key in menu:
            print(" %s : %s" % (key, menu[key]))
        answer = input("Enter a number: ")
        if answer == "q":
            exit()
        return(answer)

def GetDefnamesAsDic(me):
    defpattern = re.compile("^.*def\s")
    stoppattern = re.compile("# STOP")
    menu = {}
    itemnumber = 1
    for line in me:
        if defpattern.search(line):
            defname = (line.split(' ')[1].split('(')[0])
            menu[itemnumber] = defname
            itemnumber = itemnumber+1
        if stoppattern.search(line):
            return(menu)


def PrintAndRunDef(menu, num):
    defname = menu[int(num)]
    Drawline()
    print("Contents of %s" % defname)
    Drawline()
    lines = inspect.getsource(eval(defname))
    print(lines)
    Drawline()
    print("Executing %s" % defname)
    Drawline()
    eval(defname)()
    PrintAndRunDef(menu,DisplayMenu(menu))
    
def main(args):
    try:
        print(dir(DictionaryOfLists))
        thisfilename = args[0]
        thisfileaslist = [line.rstrip('\n') for line in open(thisfilename)]
        defsnamesasdic = GetDefnamesAsDic(thisfileaslist)
        menunum = DisplayMenu(defsnamesasdic)
        PrintAndRunDef(defsnamesasdic, menunum)

    except OSError as err:
        print("\n\nOS error: {0}".format(err))


if __name__ == '__main__':
    try:
        main(sys.argv)
    except KeyboardInterrupt:
        print('Interupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
